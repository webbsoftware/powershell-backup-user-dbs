# To-Do Items

* output total bytes in backup files.
* add support for .INI / .XML in order for addtional optional options:
  * Databases to exclude
  * Databases to include (only)
  * Services to stop before backups (and option to start)
* option to zip / compress the .bak file, indivudally or collectively
* option for alternate database authentication (maybe)



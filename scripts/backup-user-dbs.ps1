param (
    [string] $Server       = "(local)",
    [string] $WorkDir      = "",
    [string] $ShowProgress = "N"
 )

# imports
Import-Module �sqlps� -DisableNameChecking

# inits
$temp_datetime = get-date -format "yyyyMMdd_HHmmss"
$ShowProgress  = $ShowProgress.ToUpper()

# connect to SQL server; set work folder
$backup_folder = ""
$server_obj = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $Server

if ($WorkDir -eq "")
    {
        $backup_folder = Resolve-Path (Join-Path -Path $server_obj.BackupDirectory -ChildPath ("..\backup"))
        $backup_folder = $server_obj.BackupDirectory
    }
else
    {
        $backup_folder = $WorkDir
    }

if (!(Test-Path -PathType Container $backup_folder) ) 
    {
        write-host "Work directory does not exist: ", $backup_folder
        return
    }

# create sub-folder for this run
$backup_folder = Join-Path -Path $backup_folder -ChildPath $temp_datetime

if (!(Test-Path -PathType Container $backup_folder) ) 
{
    #write-host "Creating", $backup_folder
    New-Item -ItemType Directory -Force -Path $backup_folder
}

# backup user databases
$db_count = 0
foreach($sqlDatabase in $server_obj.databases)
{
    if (-not $sqlDatabase.IsSystemObject)  
    { 
        $db_count += 1
        if ($ShowProgress -match "Y") { write-host "Backing up DB:", $sqlDatabase.Name }

        $backup_filename = Join-Path -Path $backup_folder -ChildPath ($sqlDatabase.Name + ".bak")
        Backup-SqlDatabase -ServerInstance $Server -Database $sqlDatabase.Name -BackupFile $backup_filename
    }

}

write-host ""
write-host "---- Summary-----------------------------------"
write-host "Databases count: ", $db_count
write-host "Backup folder  : ", $backup_folder

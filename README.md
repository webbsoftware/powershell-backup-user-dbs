# Backup User DBs

Backup all user (non-system) databases into a single folder that is stamped with a date-time.

The intent is for a quick way to perform a backup before performing a software upgrade.

## Basic Usage

```
powershell -f backup-user-dbs.ps1
```
* Looks for for SQL Server Backup folder, creates a new folder with a date-time stamp (like 20161014_193618), and executes a database backup, saving the files as DBNAME.bak.

Depending on Powershell permissions, you may need to invoke in Bypass mode:
```
powershell -exec bypass -f backup-user-dbs.ps1
```

## Sample Output

```
---- Summary-----------------------------------
Databases count:  25
Backup folder  :  c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Backup\20161015_111748
```

## Command-Line Parameters

|Option|Description|
| ------------- |-------------|
|-ShowProgress:Y|Outputs names of databases as it progress. Default is N.|
|-Server:"servername"|Name of server. Default is "(local)"|
|-WorkDir:"parent folder"|Name of parent folder for newly-created date-time folder. Default is "", triggering the use of the SQL Server Backup folder|

## License

All software included is bundled with own license

The MIT License (MIT)

Copyright (c) 2016, Webb Software. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.